package gui.pageObjectModels.Bena.WF;

import org.openqa.selenium.WebDriver;
import com.shaft.gui.browser.BrowserActions;
import com.shaft.gui.element.ElementActions;
import org.openqa.selenium.By;

public class RegistrationPage {

	private WebDriver browserObject;
	private String URLForNavigation = "http://benaaserver.com:8080/benaa-ui/#/auth/sys-login?returnUrl=%2F";
	private String URLAfterRedirection = "http://benaaserver.com:8080/benaa-ui/#/auth/signup";
	//private String pageTitle = "Benaa";
	// Absher User Data Registration

	public RegistrationPage (WebDriver browserObject) {
		this.browserObject = browserObject;
	}

	public void navigateToURLForNavigationL() {
		//BrowserActions.navigateToURL(browserObject, URLForNavigation, URLAfterRedirection);

		BrowserActions.navigateToURL(browserObject, URLForNavigation);
	}
	public void navigateToURLAfterNavigationL() {
		BrowserActions.navigateToURL(browserObject, URLAfterRedirection);
	}
	// Absher Data
	private By AbsherSignupBTN = By.id("sys-login-signup");
	private By AbsherUsername = By.id("absher-login-username");
	private By AbsherPassword = By.id("absher-login-password");
	private By AbsherLoginBTN = By.id("absher-login-login-btn");
	private By AbsherTempPass = By.id("absher-login-temp-password");
	private By AbsherContinueBTN = By.id("absher-login-continue-btn");
	private By AbsherCompleteBTN = By.id("absher-login-complete-reg-btn");

	// Registration WorkFllow
	private By RegPersonalInfoHead = By.id("signup-head-one-btn");
	private By RegPersonTitle = By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='اللقب'])[1]/preceding::div[2]");
	//private By RegPersonTitle =By.id("reg_personTitle");
	private By RegBirthPlace =By.id("reg_birthPlace");
	private By RegBasicArea =By.id("reg_basicArea");
	private By RegCityOrGov =By.id("reg_cityOrGov");
	private By RegScientificQualification =By.id("reg_scientificQualification");
	private By RegWorkPlace =By.id("reg_workPlace");
	private By RegEmployer =By.id("reg_employer");
	private By RegMethodOfCommunication =By.id("reg_methodOfCommunication");
	//Registration Info
	private By RegRegistrationInfoHead = By.id("signup-head-two-btn");
	private By RegUsername =By.id("reg_username");
	private By RegPassword =By.id("reg_password");
	private By RegConfirmPassword =By.id("reg_confirmPassword");
	//Communication Info
	private By RegCommunicationInfoHead = By.id("signup-head-three-btn");
	private By RegEmail =By.id("reg_email");
	//Address Info
	private By RegAddressInfoHead = By.id("signup-head-four-btn");
	private By RegStreet =By.id("reg_street");
	private By RegBuildingNumber =By.id("reg_buildingNumber");
	private By RegApartmentNumber =By.id("reg_apartmentNumber");
	private By RegAdditionalNumber =By.id("reg_additionalNumber");
	private By RegPostalCode =By.id("reg_postalCode");
	//Registration Possess
	private By RegCheckBox =By.id("signup-usr-agreement");
	private By RegBTNRegister =By.id("reg_btnRegister");
	//private By RegCloseRegistration = By.id("signup-complete-close-btn");

	public void LoginwithAbsheruser() {
		ElementActions.click(browserObject, AbsherSignupBTN);
		ElementActions.type(browserObject, AbsherUsername, "nagy001");
		ElementActions.type(browserObject, AbsherPassword, "123456");
		ElementActions.click(browserObject, AbsherLoginBTN);
		ElementActions.type(browserObject, AbsherTempPass, "123456");
		ElementActions.click(browserObject, AbsherContinueBTN);
		ElementActions.click(browserObject, AbsherCompleteBTN);
		//Assert.assertEquals(test1,"absher-login-complete-reg-btn");
	}

	public void RegPersonalInfo() {
		// Personal Info
		ElementActions.click(browserObject, RegPersonalInfoHead);
		ElementActions.select(browserObject, RegPersonTitle,"صاحب السمو الأمير");
		ElementActions.select(browserObject, RegBirthPlace, "المملكة العربية السعودية");
		ElementActions.select(browserObject, RegBasicArea, "الرياض");
		ElementActions.select(browserObject, RegCityOrGov, "مدينة الرياض");
		ElementActions.select(browserObject, RegScientificQualification, "بكالوريوس");
		ElementActions.type(browserObject, RegWorkPlace, "الرياض");
		ElementActions.type(browserObject, RegEmployer, "BMC");
		ElementActions.select(browserObject, RegMethodOfCommunication, "الكل");
	}
	public void RegRegistrationInfo() {
		//Registration Info
		ElementActions.click(browserObject, RegRegistrationInfoHead);
		ElementActions.type(browserObject, RegUsername, "Owner");
		ElementActions.type(browserObject, RegPassword, "Abc@123456");
		ElementActions.type(browserObject, RegConfirmPassword, "Abc@123456");
	}
	public void RegConnectionInfo() {	
		//Communication Info
		ElementActions.click(browserObject, RegCommunicationInfoHead);
		ElementActions.type(browserObject, RegEmail, "bmc@bmc.bmc");
	}
	public void RegAddressInfo() {	
		//Address Info
		ElementActions.click(browserObject, RegAddressInfoHead);
		ElementActions.type(browserObject, RegStreet, "3071A");
		ElementActions.type(browserObject, RegBuildingNumber, "2524");
		ElementActions.type(browserObject, RegApartmentNumber, "2526");
		ElementActions.type(browserObject, RegAdditionalNumber, "1010");
		ElementActions.type(browserObject, RegPostalCode, "11595");
	}
	public void RegRegistrationPossess() {
		//Registration Possess
		ElementActions.click(browserObject, RegCheckBox);
		ElementActions.click(browserObject, RegBTNRegister);
		//ElementActions.click(browserObject, RegCloseRegistration);
	}
}
