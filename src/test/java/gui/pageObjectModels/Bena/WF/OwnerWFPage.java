package gui.pageObjectModels.Bena.WF;

import static org.testng.AssertJUnit.assertEquals;
import static org.testng.Assert.assertEquals;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.shaft.gui.browser.BrowserActions;
import com.shaft.gui.element.ElementActions;
import com.shaft.validation.Assertions;

public class OwnerWFPage {
	
	private WebDriver browserObject;
	private String URLForNavigation = "http://benaaserver.com:8080/benaa-ui/#/auth/sys-login?returnUrl=%2F";
	
	public OwnerWFPage (WebDriver browserObject) {
		this.browserObject = browserObject;
	}

	public void navigateToURLForNavigationL() {
		BrowserActions.navigateToURL(browserObject, URLForNavigation);
	}
	
	//Client Login
	private By ClientUsername = By.id("sys-login-username-input");
	private By ClientPassword = By.id("sys-login-password-input");
	private By ClientLoginBTN = By.id("sys-login-login-btn");
	//Client New Request
	private By ClientNewRequestBTN = By.xpath("/html/body/app-root/div/div/div[2]/div[2]/app-preview-build-licence/div/div/div[2]/div[2]/button/svg/path");
			/// need to change to id
	private By ClientType = By.id("Benaa_applicantRequest_applicantType");
	
	private By NextBTN1 = By.id("nextBtn");
	
	private By ClientInstrumentNumber = By.id("Benaa_mainData_instrumentNumber");//123456789
	private By ClientInstrumentDate = By.id("hijri-cal-input-instrumentDate"); //1441-02-01
	private By ClientMainDataStreet = By.id("Benaa_mainData_street");
	private By ClientMainClassification = By.id("Benaa_mainData_mainClassification");
	private By ClientSubClassification = By.id("Benaa_mainData_subClassification");
	private By ClientLandType = By.id("Benaa_mainData_landType");
	private By ClientBlockType = By.id("Benaa_mainData_blockType");
	
	private By NextBTN2 = By.id("nextBtn");
	private By NextBTN3 = By.id("nextBtn");
	
	private By EngineerOfficeAmanats = By.id("Benaa_eng_office_amanats");
	private By EngineerOfficeMunicipality = By.id("Benaa_eng_office_municipality");
	private By EngineerOfficeSelection = By.className("ui-radiobutton-icon ui-clickable");/// need to change to id
	
	private By EngineerOfficeConfirmatonYes = By.className("ui-button-text ui-clickable"); /// need to chage to id
	
	private By ClientSendRequestBTN = By.id("sendBtn");
	
	//private By ClientSendRquestSuccessMassage = By.className("col text-success");
	private By ClientCloseRequest = By.className("ui-button-text ui-clickable");

	public void OwnerWFLogin() {
		ElementActions.type(browserObject, ClientUsername, "user1");
		ElementActions.type(browserObject, ClientPassword, "123456");
		ElementActions.click(browserObject, ClientLoginBTN);
		
	}
	
	public void OwnerWFRequest() {
		ElementActions.click(browserObject, ClientNewRequestBTN);
		
		ElementActions.select(browserObject, ClientType, "مالك");
		ElementActions.click(browserObject, NextBTN1);
		
		ElementActions.type(browserObject, ClientInstrumentNumber, "123456789");
		ElementActions.type(browserObject, ClientInstrumentDate, "1441-02-01");
		ElementActions.type(browserObject, ClientMainDataStreet, "الرياض");
		ElementActions.select(browserObject, ClientMainClassification, "سكني");
		ElementActions.select(browserObject, ClientSubClassification, "فيلا سكنية منفصلة");
		ElementActions.select(browserObject, ClientLandType, "أرض مخططة");
		ElementActions.select(browserObject, ClientBlockType, "مسلح");
		ElementActions.click(browserObject, NextBTN2);
		
		ElementActions.click(browserObject, NextBTN3);
		
		ElementActions.select(browserObject, EngineerOfficeAmanats, "أمانة منطقة الرياض");
		ElementActions.select(browserObject, EngineerOfficeMunicipality, "بلدية الحيانية و البرك");
		ElementActions.click(browserObject, EngineerOfficeSelection);
		ElementActions.click(browserObject, EngineerOfficeConfirmatonYes);
		ElementActions.click(browserObject, ClientSendRequestBTN);
		ElementActions.click(browserObject, ClientCloseRequest);
	}

	
	

}
	
	
	
