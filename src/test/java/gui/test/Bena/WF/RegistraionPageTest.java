package gui.test.Bena.WF;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.shaft.gui.browser.BrowserFactory;
import com.shaft.gui.browser.BrowserFactory.BrowserType;

import gui.pageObjectModels.Bena.WF.RegistrationPage;
import io.qameta.allure.Description;

public class RegistraionPageTest {

	// Declaring WebDriver
	private WebDriver browserObject;

	// Declaring The Page Objects that will be used throughout the test class
	private RegistrationPage RegWFPage ;

	@BeforeClass(alwaysRun = true)
	public void initializeGlobalObjectsAndNavigate() {
		browserObject = BrowserFactory.getBrowser(BrowserType.GOOGLE_CHROME);
		//browserObject = BrowserFactory.getBrowser(BrowserType.MOZILLA_FIREFOX);
		//browserObject = BrowserFactory.getBrowser(BrowserType.MICROSOFT_EDGE);
		RegWFPage = new RegistrationPage(browserObject);
		RegWFPage.navigateToURLForNavigationL();
	}

	/*
	 * @Test(description = "TC001 - Login with Absher") public void
	 * NavigateToHomePage() { RegWFPage = new RegistrationPage(browserObject);
	 * RegWFPage.navigateToURLForNavigationL();
	 */
	//}
	
	
	@Test(description = "TC002 - Login with Absher")
	@Description("Given I am on the registration page, When I provide Absher Info, Then fulfill Absher data.")
	//@Severity(SeverityLevel.NORMAL)
	public void LoginwithAbsheruser() {
		RegWFPage.LoginwithAbsheruser();
	}

	@Test(description = "TC003 - Reg Personal Info")
	@Description("Given I am on the registration page, When I provide Reg Personal Info, Then fulfill personal data.")
	//@Severity(SeverityLevel.NORMAL)
	public void RegPersonalInfo() {
		//RegWFPage.navigateToURLAfterNavigationL();
		RegWFPage.RegPersonalInfo();
	}

	@Test(description = "TC004 - Reg Registration Info")
	@Description("Given I am on the registration page, When I provide Username and Password, Then fulfill credintial data.")
	//@Severity(SeverityLevel.BLOCKER)
	public void RegRegistrationInfo() {
		RegWFPage.RegRegistrationInfo();
	}

	@Test(description = "TC005 - Reg Connection Info")
	@Description("Given I am on the registration page, When I provide Reg connection Info, Then fulfill connection data.")
	//@Severity(SeverityLevel.CRITICAL)
	public void RegConnectionInfo() {
		RegWFPage.RegConnectionInfo();
	}

	@Test(description = "TC006- Reg Address Info")
	@Description("Given I am on the registration page, When I provide Reg Address Info, Then fulfill Address data.")
	//@Severity(SeverityLevel.NORMAL)
	public void RegAddressInfo() {
		RegWFPage.RegAddressInfo();
	}
	@Test(description = "TC007 - Reg Registration Possess")
	@Description("Given I am on the registration page, When I need to complete registration, Then press check box and register.")
	//@Severity(SeverityLevel.NORMAL)
	public void RegRegistrationPossess() {
		RegWFPage.RegRegistrationPossess();
	}



}

