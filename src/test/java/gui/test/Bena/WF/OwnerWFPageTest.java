package gui.test.Bena.WF;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import com.shaft.gui.browser.BrowserFactory;
import com.shaft.gui.browser.BrowserFactory.BrowserType;
import gui.pageObjectModels.Bena.WF.OwnerWFPage;
import io.qameta.allure.Description;

public class OwnerWFPageTest {

	// Declaring WebDriver
	private WebDriver browserObject;

	// Declaring The Page Objects that will be used throughout the test class
	private OwnerWFPage OwnWFPage ;

	@BeforeClass(alwaysRun = true)
	public void initializeGlobalObjectsAndNavigate() {
		browserObject = BrowserFactory.getBrowser(BrowserType.GOOGLE_CHROME);
		OwnWFPage = new OwnerWFPage (browserObject);
		OwnWFPage.navigateToURLForNavigationL();
	}
	
	@Test(description = "TC001 - Owner Login")
	@Description("Given Owner Login Data, When I need to login, Then fulfill Owner Login data.")
	//@Severity(SeverityLevel.NORMAL)
		public void OwnerWFLogin() {
			OwnWFPage.OwnerWFLogin();
		}

	@Test(description = "TC002 - Owner Build Request")
	@Description("Given Owner need to build new Beann Request, When provide need data, Then fulfill Owner data.")
	//@Severity(SeverityLevel.NORMAL)
		public void OwnerBuildRequest() {
		OwnWFPage.OwnerWFRequest();
	}
}
